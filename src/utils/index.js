export class Category {
    constructor(data) {
        this.data = data
    }

    getId() {
        return this.data.id
    }

    getName() {
        return this.data.name
    }
}

export class Product {
    constructor(data) {
        this.data = data
    }

    getId() {
        return this.data.id
    }

    getTitle() {
        return this.data.title
    }

    getImage() {
        return this.data.image
    }

    getDescription() {
        return this.data.description
    }

    getCategoryId() {
        return this.data.categoryId
    }
}